/*
	1. Create a function named getUserInfo which is able to return an object. 

		The object returned should have the following properties:
		
		- key - data type

		- name - String
		- age -  Number
		- address - String
		- isMarried - Boolean
		- petName - String

		Note: Property names given is required and should not be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.

*/
let user = "getUserInfo();"
console.log(user);

function returnUserInfo() {
	let getUserInfo = {
		Name: "John Doe",
		age: 25,
		address: "123 Street, Quezon City",
		isMarried: false,
		petname: "Donny"	}

	return getUserInfo;
}

let myUserInfo = returnUserInfo();
console.log(myUserInfo);


/*
	2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
		
		- Note: the array returned should have at least 5 elements as strings.
			    function name given is required and cannot be changed.


		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
	
*/

let user1 = "getArtistArray();"
console.log(user1);

function getArtistArray(){
	let artistArray = ["Ben & Ben", "Dilaw", "Bright-Win", "Callalily", "Taylor Swift"];


	return artistArray;
}

let getArtArray = getArtistArray();
console.log(getArtArray);


/*
	3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/

let user2 = "getSongsArray();"
console.log(user2);

function getSongsArray(){
	let songsArray = ["Kathang Isip", "Uhaw", "2gther the series OST", "Magbalik", "Love Story"];


	return songsArray;
}

let getSongArray = getSongsArray();
console.log(getSongArray);



/*
	4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/

let user3 = "getMoviesArray();"
console.log(user3);

function getMoviesArray(){
	let moviesArray = ["Titanic", "Spiderman Home Coming", "Avengers End Game", "Harry Potter Volume 1-5", "Dr. Strange"];


	return moviesArray;
}

let getMovsArray = getMoviesArray();
console.log(getMovsArray);



/*
	5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

			- Note: the array returned should have numbers only.
			        function name given is required and cannot be changed.

			To check, create a variable to save the value returned by the function.
			Then log the variable in the console.

			Note: This is optional.
			
*/

let user4 = "getPrimeNumberArray();"
console.log(user4);

function getPrimeNumberArray(){
	let primeNumArray = [2, 3, 4, 5, 6];


	return primeNumArray;
}

let getPrimNoArray = getPrimeNumberArray();
console.log(getPrimNoArray);



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
		getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
		getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
		getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
		getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

	}
} catch(err){


}